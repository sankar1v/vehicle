package com.san.vehicle.service;

import com.san.vehicle.config.PropertyConfig;
import com.san.vehicle.model.RegDtl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.san.vehicle.util.VehicleConstants.INPUT_FOLDER;
import static com.san.vehicle.util.VehicleConstants.OUTPUT_FOLDER;

/**
 * Created by sankarvinnakota on 30/11/19.
 */
@Service
@Slf4j
public class ProcessExcelService {

    @Autowired
    private PropertyConfig propertyConfig;

    public List<String> readExcelAndGetRegNos() {
        List<String> regNoList = new ArrayList<>();
        String inputPathAndFileName = propertyConfig.getFilePath() + INPUT_FOLDER + propertyConfig.getInputFileName();
        try {
            FileInputStream excelFile = new FileInputStream(new File(inputPathAndFileName));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            if(iterator.hasNext()) {
                iterator.next(); // Skip header
            }
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                while (cellIterator.hasNext()) {
                    Cell currentCell = cellIterator.next();
                    if(StringUtils.isNotEmpty(currentCell.getStringCellValue())) {
                        regNoList.add(currentCell.getStringCellValue());
                        //log.info("Current cell value:{}", currentCell.getStringCellValue());
                    }
                }
            }
            workbook.close();
        } catch (Exception e) {
            log.error("Exception while reading input excel file:{}", inputPathAndFileName, e);
        }
        log.info("Fetched Registration No.s from Excel {}", regNoList);
        return regNoList;
    }

    public void writeExcel(RegDtl regDtl) {
        String outputPathAndFileName = propertyConfig.getFilePath() + OUTPUT_FOLDER + propertyConfig.getOutputFileName();

        try {
            FileInputStream inputStream = new FileInputStream(new File(outputPathAndFileName));
            Workbook workbook = WorkbookFactory.create(inputStream);
            Sheet sheet = workbook.getSheetAt(0);
            int rowCount = sheet.getLastRowNum();

            Row row = sheet.createRow(++rowCount);
            int columnCount = 0;

            /*
            Cell cell = row.createCell(columnCount);
            cell.setCellValue(rowCount);
            for (Object field : aBook) {
                cell = row.createCell(columnCount++);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
            }*/

            {
                row.createCell(columnCount++).setCellValue(regDtl.getRegisteredOwner());
                row.createCell(columnCount++).setCellValue(regDtl.getRegistrationNo());
                row.createCell(columnCount++).setCellValue(regDtl.getPresentAddress());
                row.createCell(columnCount++).setCellValue(regDtl.getMobileNo());
                row.createCell(columnCount++).setCellValue(regDtl.getHypothecatedTo());
                row.createCell(columnCount++).setCellValue(regDtl.getChasisNo());
                row.createCell(columnCount++).setCellValue(regDtl.getEngineNumber());
                row.createCell(columnCount++).setCellValue(regDtl.getVehicleClass());
                row.createCell(columnCount++).setCellValue(regDtl.getMakersName());
                row.createCell(columnCount++).setCellValue(regDtl.getMakersClassification());
                row.createCell(columnCount++).setCellValue(regDtl.getApplicationNo());
                row.createCell(columnCount++).setCellValue(regDtl.getPolicyNumber());
                row.createCell(columnCount++) .setCellValue(regDtl.getPolicyValidTill());
                row.createCell(columnCount++).setCellValue(regDtl.getRegisteringAuthority());
            }

            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(outputPathAndFileName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();

            log.info("Saved record to excel:{} with Registration No:{}, Chasis No: {}, Application No:{}",
                    outputPathAndFileName, regDtl.getRegistrationNo(), regDtl.getChasisNo(), regDtl.getApplicationNo());
        } catch (IOException | EncryptedDocumentException
                | InvalidFormatException e) {
            log.error("Exception while saving record:{} to excel:{}", regDtl, outputPathAndFileName, e);
        }
    }

}
