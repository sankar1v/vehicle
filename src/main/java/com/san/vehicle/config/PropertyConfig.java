package com.san.vehicle.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by sankarvinnakota on 01/12/19.
 */
@ConfigurationProperties
@Configuration
@Data
public class PropertyConfig {

    @Value("${file.path:/Users/sankarvinnakota/Desktop/Personal/git/vehicle/}")
    private String filePath;

    @Value("${input.fileName:input.xlsx}")
    private String inputFileName;

    @Value("${output.fileName:output.xlsx}")
    private String outputFileName;

    @Value("${sleep.time.seconds:10}")
    private long sleepTimeSecs;

    @Value("${fetch.cars.only:true}")
    private boolean fetchCarsOnly;

}
