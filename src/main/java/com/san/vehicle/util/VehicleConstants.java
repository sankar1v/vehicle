package com.san.vehicle.util;

/**
 * Created by sankarvinnakota on 01/12/19.
 */
public class VehicleConstants {

    public static final String GET_CHASIS_NO_BY_REG_NO_URL = "https://rtaappsc.epragathi.org:1201/reg/citizenServices/getOtherStateTPDetails?prNo=";
    public static final String GET_APPLICATION_NO_URL = "https://rtaappsc.epragathi.org:1201/reg/citizenServices/applicationSearchForServicesRegisration";
    public static final String DOWNLOAD_PDF_URL = "https://rtaappsc.epragathi.org:1201/reg/citizenServices/getregcitizenprdetails?applicationNo=";

    public static final String VEHICLE_TYPE_CAR = "MCRN";
    public static final String PDF_FOLDER = "pdf/";

    public static final String INPUT_FOLDER = "input/";
    public static final String OUTPUT_FOLDER = "output/";

    public static final String PDF_EXTENSION = ".pdf";

    public static final String MOBILE_NO = "Mobile No : ";
    public static final String EMPTY = "";
    public static final String HYPOTHECATED_TO = "Hypothecated To";
    public static final String COLON_SPACE = ": ";
    public static final String MAKERS_NAME = "Makers Name : ";
    public static final String ENGINE_POWER = " Engine Power : ";
    public static final String MAKERS = "Maker's ";
    public static final String REGISTERTING_AUTORITY = "Registering Authority";

}
