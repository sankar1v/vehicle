
package com.san.vehicle.model.application;

import java.util.Map;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "applicationNumber",
    "applicantName",
    "prNo",
    "trNo",
    "applicationStatus",
    "fatherName",
    "tokenCancelRequired",
    "otherSateCOVTOIsdone",
    "nocVerificationDone",
    "osPaymentStatus",
    "uploadTrCopy",
    "feedBackFormsubmited",
    "needToEnableSecondButton",
    "demandNoticeverified",
    "financerRequired",
    "hpadone",
    "htpdone"
})
@Data
public class Result {

    @JsonProperty("applicationNumber")
    private String applicationNumber;
    @JsonProperty("applicantName")
    private String applicantName;
    @JsonProperty("prNo")
    private String prNo;
    @JsonProperty("trNo")
    private String trNo;
    @JsonProperty("applicationStatus")
    private String applicationStatus;
    @JsonProperty("fatherName")
    private String fatherName;
    @JsonProperty("tokenCancelRequired")
    private Boolean tokenCancelRequired;
    @JsonProperty("otherSateCOVTOIsdone")
    private Boolean otherSateCOVTOIsdone;
    @JsonProperty("nocVerificationDone")
    private Boolean nocVerificationDone;
    @JsonProperty("osPaymentStatus")
    private Boolean osPaymentStatus;
    @JsonProperty("uploadTrCopy")
    private Boolean uploadTrCopy;
    @JsonProperty("feedBackFormsubmited")
    private Boolean feedBackFormsubmited;
    @JsonProperty("needToEnableSecondButton")
    private Boolean needToEnableSecondButton;
    @JsonProperty("demandNoticeverified")
    private Boolean demandNoticeverified;
    @JsonProperty("financerRequired")
    private Boolean financerRequired;
    @JsonProperty("hpadone")
    private Boolean hpadone;
    @JsonProperty("htpdone")
    private Boolean htpdone;
    @JsonProperty("additionalProperties")
    private Map<String, Object> additionalProperties;

}
