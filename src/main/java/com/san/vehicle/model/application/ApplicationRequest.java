package com.san.vehicle.model.application;

import java.util.Map;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "trNo",
        "module",
        "applicationNo",
        "chassisNo",
        "prNo"
})
/**
 * Created by sankarvinnakota on 01/12/19.
 */
@Data
@NoArgsConstructor
public class ApplicationRequest {

    @JsonProperty("trNo")
    private String trNo = "";
    @JsonProperty("module")
    private String module = "REG";
    @JsonProperty("applicationNo")
    private String applicationNo = "";
    @JsonProperty("chassisNo")
    private String chassisNo;
    @JsonProperty("prNo")
    private String prNo;
    @JsonProperty("additionalProperties")
    private Map<String, Object> additionalProperties;

    public ApplicationRequest(String prNo, String chasisNo) {
        this.prNo = prNo;
        this.chassisNo = chasisNo;
    }

}