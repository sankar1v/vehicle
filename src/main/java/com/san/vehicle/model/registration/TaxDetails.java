
package com.san.vehicle.model.registration;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "taxValTo",
    "otherStateTaxValUpto"
})
public class TaxDetails {

    @JsonProperty("taxValTo")
    private String taxValTo;
    @JsonProperty("otherStateTaxValUpto")
    private String otherStateTaxValUpto;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("taxValTo")
    public String getTaxValTo() {
        return taxValTo;
    }

    @JsonProperty("taxValTo")
    public void setTaxValTo(String taxValTo) {
        this.taxValTo = taxValTo;
    }

    @JsonProperty("otherStateTaxValUpto")
    public String getOtherStateTaxValUpto() {
        return otherStateTaxValUpto;
    }

    @JsonProperty("otherStateTaxValUpto")
    public void setOtherStateTaxValUpto(String otherStateTaxValUpto) {
        this.otherStateTaxValUpto = otherStateTaxValUpto;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
