
package com.san.vehicle.model.registration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "applicantDetails",
    "vehicleDetails",
    "insuranceDetails",
    "prNo",
    "classOfVehicle",
    "taxDetails",
    "classOfVehicleVO"
})
public class Result {

    @JsonProperty("applicantDetails")
    private ApplicantDetails applicantDetails;
    @JsonProperty("vehicleDetails")
    private VehicleDetails vehicleDetails;
    @JsonProperty("insuranceDetails")
    private InsuranceDetails insuranceDetails;
    @JsonProperty("prNo")
    private String prNo;
    @JsonProperty("classOfVehicle")
    private String classOfVehicle;
    @JsonProperty("taxDetails")
    private TaxDetails taxDetails;
    @JsonProperty("classOfVehicleVO")
    private List<ClassOfVehicleVO> classOfVehicleVO = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("applicantDetails")
    public ApplicantDetails getApplicantDetails() {
        return applicantDetails;
    }

    @JsonProperty("applicantDetails")
    public void setApplicantDetails(ApplicantDetails applicantDetails) {
        this.applicantDetails = applicantDetails;
    }

    @JsonProperty("vehicleDetails")
    public VehicleDetails getVehicleDetails() {
        return vehicleDetails;
    }

    @JsonProperty("vehicleDetails")
    public void setVehicleDetails(VehicleDetails vehicleDetails) {
        this.vehicleDetails = vehicleDetails;
    }

    @JsonProperty("insuranceDetails")
    public InsuranceDetails getInsuranceDetails() {
        return insuranceDetails;
    }

    @JsonProperty("insuranceDetails")
    public void setInsuranceDetails(InsuranceDetails insuranceDetails) {
        this.insuranceDetails = insuranceDetails;
    }

    @JsonProperty("prNo")
    public String getPrNo() {
        return prNo;
    }

    @JsonProperty("prNo")
    public void setPrNo(String prNo) {
        this.prNo = prNo;
    }

    @JsonProperty("classOfVehicle")
    public String getClassOfVehicle() {
        return classOfVehicle;
    }

    @JsonProperty("classOfVehicle")
    public void setClassOfVehicle(String classOfVehicle) {
        this.classOfVehicle = classOfVehicle;
    }

    @JsonProperty("taxDetails")
    public TaxDetails getTaxDetails() {
        return taxDetails;
    }

    @JsonProperty("taxDetails")
    public void setTaxDetails(TaxDetails taxDetails) {
        this.taxDetails = taxDetails;
    }

    @JsonProperty("classOfVehicleVO")
    public List<ClassOfVehicleVO> getClassOfVehicleVO() {
        return classOfVehicleVO;
    }

    @JsonProperty("classOfVehicleVO")
    public void setClassOfVehicleVO(List<ClassOfVehicleVO> classOfVehicleVO) {
        this.classOfVehicleVO = classOfVehicleVO;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
