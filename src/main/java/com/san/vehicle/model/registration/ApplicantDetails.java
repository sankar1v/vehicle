
package com.san.vehicle.model.registration;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "displayName",
    "contact",
    "presentAddress",
    "permanantAddress",
    "aadharResponse",
    "applicantionType",
    "qualification",
    "bloodGrp",
    "sameAsAadhar",
    "isAvailablePresentAddrsProof"
})
public class ApplicantDetails {

    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("contact")
    private Contact contact;
    @JsonProperty("presentAddress")
    private PresentAddress presentAddress;
    @JsonProperty("permanantAddress")
    private PermanantAddress permanantAddress;
    @JsonProperty("aadharResponse")
    private AadharResponse aadharResponse;
    @JsonProperty("applicantionType")
    private ApplicantionType applicantionType;
    @JsonProperty("qualification")
    private Qualification qualification;
    @JsonProperty("bloodGrp")
    private BloodGrp bloodGrp;
    @JsonProperty("sameAsAadhar")
    private Boolean sameAsAadhar;
    @JsonProperty("isAvailablePresentAddrsProof")
    private Boolean isAvailablePresentAddrsProof;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("displayName")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("displayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("contact")
    public Contact getContact() {
        return contact;
    }

    @JsonProperty("contact")
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @JsonProperty("presentAddress")
    public PresentAddress getPresentAddress() {
        return presentAddress;
    }

    @JsonProperty("presentAddress")
    public void setPresentAddress(PresentAddress presentAddress) {
        this.presentAddress = presentAddress;
    }

    @JsonProperty("permanantAddress")
    public PermanantAddress getPermanantAddress() {
        return permanantAddress;
    }

    @JsonProperty("permanantAddress")
    public void setPermanantAddress(PermanantAddress permanantAddress) {
        this.permanantAddress = permanantAddress;
    }

    @JsonProperty("aadharResponse")
    public AadharResponse getAadharResponse() {
        return aadharResponse;
    }

    @JsonProperty("aadharResponse")
    public void setAadharResponse(AadharResponse aadharResponse) {
        this.aadharResponse = aadharResponse;
    }

    @JsonProperty("applicantionType")
    public ApplicantionType getApplicantionType() {
        return applicantionType;
    }

    @JsonProperty("applicantionType")
    public void setApplicantionType(ApplicantionType applicantionType) {
        this.applicantionType = applicantionType;
    }

    @JsonProperty("qualification")
    public Qualification getQualification() {
        return qualification;
    }

    @JsonProperty("qualification")
    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    @JsonProperty("bloodGrp")
    public BloodGrp getBloodGrp() {
        return bloodGrp;
    }

    @JsonProperty("bloodGrp")
    public void setBloodGrp(BloodGrp bloodGrp) {
        this.bloodGrp = bloodGrp;
    }

    @JsonProperty("sameAsAadhar")
    public Boolean getSameAsAadhar() {
        return sameAsAadhar;
    }

    @JsonProperty("sameAsAadhar")
    public void setSameAsAadhar(Boolean sameAsAadhar) {
        this.sameAsAadhar = sameAsAadhar;
    }

    @JsonProperty("isAvailablePresentAddrsProof")
    public Boolean getIsAvailablePresentAddrsProof() {
        return isAvailablePresentAddrsProof;
    }

    @JsonProperty("isAvailablePresentAddrsProof")
    public void setIsAvailablePresentAddrsProof(Boolean isAvailablePresentAddrsProof) {
        this.isAvailablePresentAddrsProof = isAvailablePresentAddrsProof;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
