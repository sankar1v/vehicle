
package com.san.vehicle.model.registration;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cid",
    "covcode",
    "covdescription",
    "category",
    "panrequired",
    "invalidCov",
    "isSecondVehicle",
    "covCode"
})
public class ClassOfVehicleVO {

    @JsonProperty("cid")
    private Integer cid;
    @JsonProperty("covcode")
    private String covcode;
    @JsonProperty("covdescription")
    private String covdescription;
    @JsonProperty("category")
    private String category;
    @JsonProperty("panrequired")
    private Boolean panrequired;
    @JsonProperty("invalidCov")
    private Boolean invalidCov;
    @JsonProperty("isSecondVehicle")
    private Boolean isSecondVehicle;
    @JsonProperty("covCode")
    private String covCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cid")
    public Integer getCid() {
        return cid;
    }

    @JsonProperty("cid")
    public void setCid(Integer cid) {
        this.cid = cid;
    }

    @JsonProperty("covcode")
    public String getCovcode() {
        return covcode;
    }

    @JsonProperty("covcode")
    public void setCovcode(String covcode) {
        this.covcode = covcode;
    }

    @JsonProperty("covdescription")
    public String getCovdescription() {
        return covdescription;
    }

    @JsonProperty("covdescription")
    public void setCovdescription(String covdescription) {
        this.covdescription = covdescription;
    }

    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    @JsonProperty("panrequired")
    public Boolean getPanrequired() {
        return panrequired;
    }

    @JsonProperty("panrequired")
    public void setPanrequired(Boolean panrequired) {
        this.panrequired = panrequired;
    }

    @JsonProperty("invalidCov")
    public Boolean getInvalidCov() {
        return invalidCov;
    }

    @JsonProperty("invalidCov")
    public void setInvalidCov(Boolean invalidCov) {
        this.invalidCov = invalidCov;
    }

    @JsonProperty("isSecondVehicle")
    public Boolean getIsSecondVehicle() {
        return isSecondVehicle;
    }

    @JsonProperty("isSecondVehicle")
    public void setIsSecondVehicle(Boolean isSecondVehicle) {
        this.isSecondVehicle = isSecondVehicle;
    }

    @JsonProperty("covCode")
    public String getCovCode() {
        return covCode;
    }

    @JsonProperty("covCode")
    public void setCovCode(String covCode) {
        this.covCode = covCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
