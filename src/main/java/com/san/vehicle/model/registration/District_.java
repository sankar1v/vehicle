
package com.san.vehicle.model.registration;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "createdBy",
    "modifiedBy",
    "modifiedDate",
    "districCode",
    "districtId",
    "districtName",
    "stateId",
    "status",
    "zonecode",
    "name"
})
public class District_ {

    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("modifiedBy")
    private String modifiedBy;
    @JsonProperty("modifiedDate")
    private String modifiedDate;
    @JsonProperty("districCode")
    private Integer districCode;
    @JsonProperty("districtId")
    private Integer districtId;
    @JsonProperty("districtName")
    private String districtName;
    @JsonProperty("stateId")
    private String stateId;
    @JsonProperty("status")
    private String status;
    @JsonProperty("zonecode")
    private String zonecode;
    @JsonProperty("name")
    private String name;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @JsonProperty("modifiedBy")
    public String getModifiedBy() {
        return modifiedBy;
    }

    @JsonProperty("modifiedBy")
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @JsonProperty("modifiedDate")
    public String getModifiedDate() {
        return modifiedDate;
    }

    @JsonProperty("modifiedDate")
    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @JsonProperty("districCode")
    public Integer getDistricCode() {
        return districCode;
    }

    @JsonProperty("districCode")
    public void setDistricCode(Integer districCode) {
        this.districCode = districCode;
    }

    @JsonProperty("districtId")
    public Integer getDistrictId() {
        return districtId;
    }

    @JsonProperty("districtId")
    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    @JsonProperty("districtName")
    public String getDistrictName() {
        return districtName;
    }

    @JsonProperty("districtName")
    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    @JsonProperty("stateId")
    public String getStateId() {
        return stateId;
    }

    @JsonProperty("stateId")
    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("zonecode")
    public String getZonecode() {
        return zonecode;
    }

    @JsonProperty("zonecode")
    public void setZonecode(String zonecode) {
        this.zonecode = zonecode;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
