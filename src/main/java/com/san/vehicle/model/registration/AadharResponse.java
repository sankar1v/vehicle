
package com.san.vehicle.model.registration;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "insideAP",
    "stateMatched",
    "districtMatched",
    "mandalMatched"
})
public class AadharResponse {

    @JsonProperty("insideAP")
    private Boolean insideAP;
    @JsonProperty("stateMatched")
    private Boolean stateMatched;
    @JsonProperty("districtMatched")
    private Boolean districtMatched;
    @JsonProperty("mandalMatched")
    private Boolean mandalMatched;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("insideAP")
    public Boolean getInsideAP() {
        return insideAP;
    }

    @JsonProperty("insideAP")
    public void setInsideAP(Boolean insideAP) {
        this.insideAP = insideAP;
    }

    @JsonProperty("stateMatched")
    public Boolean getStateMatched() {
        return stateMatched;
    }

    @JsonProperty("stateMatched")
    public void setStateMatched(Boolean stateMatched) {
        this.stateMatched = stateMatched;
    }

    @JsonProperty("districtMatched")
    public Boolean getDistrictMatched() {
        return districtMatched;
    }

    @JsonProperty("districtMatched")
    public void setDistrictMatched(Boolean districtMatched) {
        this.districtMatched = districtMatched;
    }

    @JsonProperty("mandalMatched")
    public Boolean getMandalMatched() {
        return mandalMatched;
    }

    @JsonProperty("mandalMatched")
    public void setMandalMatched(Boolean mandalMatched) {
        this.mandalMatched = mandalMatched;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
