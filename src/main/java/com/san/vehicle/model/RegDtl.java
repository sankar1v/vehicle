package com.san.vehicle.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by sankarvinnakota on 01/12/19.
 */
@Data
@NoArgsConstructor
@ToString
public class RegDtl {
    String registrationNo;
    String chasisNo;
    String applicationNo;
    String registeredOwner; // result.applicantName from applicationSearchForServicesRegisration
    String engineNumber;

    String policyNumber;
    String policyValidTill;
    String classOfVehicle; // MCRN for car
    String vehicleClass; // covdescription

    String presentAddress; // presentAddress.doorNo

    // To be extracted from pdf
    String mobileNo;
    String hypothecatedTo;
    String makersName;
    String makersClassification;
    String registeringAuthority;

    public RegDtl(String registrationNo) {
        this.registrationNo = registrationNo;
    }
}
